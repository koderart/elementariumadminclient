package ru.muvs.admin.main {

import flash.events.ErrorEvent;
import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestMethod;
import flash.utils.ByteArray;
import flash.utils.Dictionary;

import mx.controls.Alert;


public class RESTLoader {

    private static var _instance: RESTLoader;

    private var requestsDictionary:Dictionary = new Dictionary(true);
    
    public static function get instance():RESTLoader {
        if (_instance == null) {
            _instance = new RESTLoader();
        }
        return _instance;
    }

    public function RESTLoader() {
    }

    public function sendRequest(url:String, resultHandler:Function, method:String = URLRequestMethod.GET, data:Object = null):void {
        var request:URLRequest = new URLRequest(url);
        request.method = method;
        request.contentType = "application/json";
        if (data) {
            request.data = JSON.stringify(data);
        }
        var loader:URLLoader = new URLLoader();
        loader.addEventListener(Event.COMPLETE, loaderResultHandler, false, 0, true);
        loader.addEventListener(IOErrorEvent.IO_ERROR, httpRequestError, false, 0, true);
        loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, httpRequestError, false, 0, true);
        loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler, false, 0, true);
        loader.load(request);

        if (resultHandler != null) {
            requestsDictionary[loader] = resultHandler;
        } else {
            requestsDictionary[loader] = true;
        }
    }

    public function sendRequestBinary(url:String, resultHandler:Function, data:ByteArray):void {
        var request:URLRequest = new URLRequest(url);
        request.method = URLRequestMethod.POST;
        //request.contentType = "binary/octet-stream";
        request.contentType = "application/json";
        request.data = data;
        var loader:URLLoader = new URLLoader();
        //loader.dataFormat = URLLoaderDataFormat.BINARY;
        loader.addEventListener(Event.COMPLETE, loaderResultHandler, false, 0, true);
        loader.addEventListener(IOErrorEvent.IO_ERROR, httpRequestError, false, 0, true);
        loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, httpRequestError, false, 0, true);
        loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler, false, 0, true);
        loader.load(request);

        if (resultHandler != null) {
            requestsDictionary[loader] = resultHandler;
        } else {
            requestsDictionary[loader] = true;
        }
    }

    private function loaderResultHandler(event:Event):void {
        var loader:URLLoader = event.target as URLLoader;
        loader.removeEventListener(Event.COMPLETE, loaderResultHandler);
        loader.removeEventListener(IOErrorEvent.IO_ERROR, httpRequestError);
        loader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, httpRequestError);
        loader.removeEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);

        var data:Object;
        if (loader.data && loader.data is String) {
            try {
                data = JSON.parse(loader.data);
            } catch (e:Error) {
                Alert.show("Ожидался JSON а пришел текст:"+loader.data, "Ошибка сервера");
                return;
            }

            if (!(data is Number) && data.error) {
                Alert.show(data.error, "Ошибка сервера");
                return;
            }
        } else {
            data = loader.data;
        }

        if (requestsDictionary[loader] && requestsDictionary[loader] is Function) {
            requestsDictionary[loader].call(null, data);
        }

        delete requestsDictionary[loader];
    }

    private function httpRequestError(error:ErrorEvent):void {
        delete requestsDictionary[error.target];
    }

    private function httpStatusHandler(event:HTTPStatusEvent):void {
        if (event.status == 500) {
            delete requestsDictionary[event.target];
            Alert.show("Server error 500");
        }
    }
}
}
