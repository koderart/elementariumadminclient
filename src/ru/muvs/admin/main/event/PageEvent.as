package ru.muvs.admin.main.event
{
	import flash.events.Event;
	
	public class PageEvent extends Event
	{
		public static const CHANGE:String = "PageEvent_CHANGE";
		
		public var page:int;

		
		public function PageEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}