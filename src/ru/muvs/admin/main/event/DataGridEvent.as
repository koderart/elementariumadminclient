package ru.muvs.admin.main.event
{
	import flash.events.Event;
	
	public class DataGridEvent extends Event
	{
		public static const DELETE:String = "DELETE_DataGridEvent";
		public static const CLONE:String = "CLONE_DataGridEvent";
		
		public var data:Object;

		
		public function DataGridEvent(type:String, bubbles:Boolean=true, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		
	}
}