package ru.muvs.admin.main {

import flash.utils.ByteArray;

import ru.muvs.admin.main.vo.enum.*;

import spark.components.gridClasses.GridColumn;

public class Utils {

    public static function clone( source:Object ):*
    {
        var myBA:ByteArray = new ByteArray();
        myBA.writeObject( source );
        myBA.position = 0;
        return( myBA.readObject() );
    }

    public static function objectToClass( toClass:Class, obj:Object ):*
    {
        var item:* = new toClass();
        for (var prop:String in obj) {
            item[prop] = obj[prop];
        }
        return item;
    }

    public static function objectsToClassArray( toClass:Class, arr:Array ):Array
    {
        var resultArray:Array = [];
        
        for each (var obj:Object in arr) {
            resultArray.push(objectToClass(toClass, obj));
        }
        
        return resultArray;
    }
    
    public static function testFileName( str:String ):Boolean
    {
        var pattern:RegExp = /^[-_A-Za-z0-9]+$/g;
        return pattern.exec(str);
    }

    public static function colorLabel(obj:Object):String {
        return ColorEnum.LABELS[obj];
    }

    public static  function slotLabel(obj:Object):String {
        return SlotEnum.LABELS[obj];
    }

    public static function slotObjLabel(obj:Object, column:GridColumn):String {
        return SlotEnum.LABELS[obj.artikulSlot];
    }

    public static function elementLabel(obj:Object):String {
        return ElementEnum.LABELS[obj];
    }

    public static function elementObjLabel(obj:Object, column:GridColumn):String {
        return ElementEnum.LABELS[obj.element];
    }

    public static function bonusActionLabel(obj:Object):String {
        return BonusActionEnum.LABELS[obj];
    }

    public static function questGoalLabel(obj:Object):String {
        return QuestGoalEnum.LABELS[obj];
    }

    public static function questGoalObjLabel(obj:Object, column:GridColumn):String {
        return QuestGoalEnum.LABELS[obj.GOAL];
    }

    public static function questLockLabel(obj:Object):String {
        return QuestLockEnum.LABELS[obj];
    }

    public static function bonusActionObjLabel(obj:Object, column:GridColumn):String {
        return BonusActionEnum.LABELS[obj.ACTION];
    }
    
    public static function bonusLockLabel(obj:Object):String {
        return BonusLockEnum.LABELS[obj];
    }

    public static function bonusLockObjLabel(obj:Object, column:GridColumn):String {
        return BonusLockEnum.LABELS[obj.LOCK];
    }

    public static function objTitleLabel(obj:Object):String {
        return obj.title.ru;
    }

    public static function levelLabel(obj:Object):String {
        if (obj.levelId) {
            return obj.number;
        } else {
            return "";
        }
    }

    public static function levelObjLabel(obj:Object, column:GridColumn):String {
        if (obj.level && obj.level.levelId) {
            return obj.level.number;
        } else {
            return "";
        }
    }


}
}
